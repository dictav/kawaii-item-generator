require 'amazon/ecs'

#Usage: bundle exec ruby wish_list.rb
module Amazon
	class Ecs
		def self.list_lookup(list_id, opts={})
			opts[:operation] = 'ListLookup'
      opts[:list_id] = list_id	

			self.send_request(opts)
		end
	end
end

include Amazon

# setup Amazon ECS
Amazon::Ecs.options = {
	:associate_tag => 'dictav-22',
	:AWS_access_key_id => '1Y19WXX4D385R0VBCQR2',       
	:AWS_secret_key => 'YHyUOi7h+USWsDIPQ1wLChpPlmJdK3xPKiENdbzd'
}


options = {
	ResponseGroup: "Images, Small, OfferSummary",
	country: 'jp'
}

ids = %w(3P37HNE8QE8CA 3W4CTKVJM2SN8 1XLHR1LTUK1RZ PH9I3YO37FXH)
items = ids.map do |id|
	response = Amazon::Ecs.list_lookup(id,options)
	response.items
end

p items
