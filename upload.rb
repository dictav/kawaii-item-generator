require 'redis'
require 'msgpack'
require 'uri'
require './local_user.rb'
require 'resolv'

n = ARGV[0]
abort "Usage: ruby upload.rb <num>" unless n

#resolv
result = Resolv::DNS.new.getresources("ks-main.dictav.net", Resolv::DNS::Resource::IN::SRV).sample
redis = Redis.new host:result.target, port:result.port, password:"mxU8YKperHQ230vW"


puts "uploading #{n} items..."

dir = "kawaii-items"
count = redis.llen :items

puts "start from #{count}"
items = []
n.to_i.times do |n|
	fn = File.join dir, "#{count + n}.dat"
	items[n] = File.read fn rescue break
end
rtn = redis.rpush :items, items
puts "Add #{rtn} items"

#redis.rpush :users, LOCALUSER.to_msgpack
