require 'amazon/ecs'
require 'msgpack'
require 'csv'

csvfile = ARGV[0]
abort "Usage: bundle exec ruby fetch_amazon_item.rb <csv_file>" unless csvfile

# params
outdir = 'kawaii-items'
ITEM_SIZE = 10
item_id = if File.exist? outdir
						 Dir["#{outdir}/*"].size
					 else 
						 Dir.mkdir outdir
						 0
					 end

puts "start fetching from #{item_id}"

# setup Amazon ECS
Amazon::Ecs.options = {
	:associate_tag => 'jkekawa-22',
	:AWS_access_key_id => '1Y19WXX4D385R0VBCQR2',       
	:AWS_secret_key => 'YHyUOi7h+USWsDIPQ1wLChpPlmJdK3xPKiENdbzd'
}
#Access Key ID: AKIAINTSXRQYKXVLXOSQ
#Secret Access Key: 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
#Amazon::Ecs.options = {
#	:associate_tag => 'jkekawa-22',
#	:AWS_access_key_id => 'AKIAINTSXRQYKXVLXOSQ',       
#	:AWS_secret_key => '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'
#}

# read CSV
keys, *lines = CSV.parse File.read(csvfile)
arr = lines.map{|a| [keys,a].transpose}.map{|a| Hash[*a.flatten]}
all_ids = arr.map{|a| a['id']}
if all_ids.nil? or all_ids.empty?
	puts 'FORMAT ERROR'
	exit(1)
end

def fetch(ids=[])
	puts "fetch " + ids.to_s
	options = {
		ResponseGroup: "Images, Small, OfferSummary",
		country: 'jp'
	}

	response = Amazon::Ecs.item_lookup(ids.join(','),options)
	response.items
end

#fetch maximum ten
print 'Start fetching'
all_ids.each_slice( ITEM_SIZE ).each do |ids|
	print '.'
	#fetch by id
	items = fetch(ids)

	if items.nil? or items.empty?
		puts "Could not fetch items"
		return
	end

	# save to file
	items.each do |item|
		id = item.get "ASIN"

		#prepare parameters
		# category
		# season
		# sex 
		# color 
		category,season,sex,color = nil
		arr.each do |a|
			if a['id'] == id
				category = a['category']
				season = a['season']
				sex = a['sex']
				color = a['color']
				break;
			end
		end


		#prepare price
		price = item.get('OfferSummary/LowestNewPrice/Amount') || 0

		#prepare count
		count = item.get('OfferSummary/TotalNew')

		#prepare data
		data = {
			name: item.get('ItemAttributes/Title'),
			url: item.get('DetailPageURL'),
			picture: item.get('LargeImage/URL') ,
			count: count.to_i,
			price: price.to_i,
			category: category,
			season: season,
			sex: sex,
			color: color
		}
		if data.values.include? nil
			puts "has nil value"
			p data
			next
		end

		# save to file
		file_name = outdir + "/#{item_id}.dat"
		File.open(file_name, 'w'){|f| f.write data.to_msgpack}
		item_id += 1
	end

end

puts 'done'


